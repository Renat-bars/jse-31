package ru.tsc.almukhametov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.IService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Optional<User> findByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    @NotNull
    Optional<User> findByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    @NotNull
    Optional<User> removeUser(@Nullable User user);

    @NotNull
    Optional<User> removeUserById(@Nullable String id);

    @NotNull
    Optional<User> removeUserByLogin(@Nullable String login);

    @NotNull
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User setRole(@Nullable String userId, @Nullable Role role);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}