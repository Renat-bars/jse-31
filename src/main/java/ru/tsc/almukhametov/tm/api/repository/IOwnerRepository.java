package ru.tsc.almukhametov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    @NotNull
    E add(@NotNull String userId, @Nullable E entity);

    void remove(@NotNull String userId, @Nullable E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @Nullable Comparator<E> comparator);

    void clear(@NotNull String userId);

    @Nullable
    Optional<E> findById(@NotNull String userId, @Nullable final String id);

    @Nullable
    Optional<E> findByIndex(@NotNull String userId, final Integer index);

    @Nullable
    Optional<E> removeById(@NotNull String userId, @Nullable final String id);

    @Nullable
    Optional<E> removeByIndex(@NotNull String userId, final Integer index);

    boolean existById(@NotNull String userId, @Nullable final String id);

    boolean existByIndex(@NotNull String userId, final int index);

}
