package ru.tsc.almukhametov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Role {

    @NotNull
    USER("User"),
    @NotNull
    ADMIN("Admin");

    @NotNull
    @Getter
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

}
