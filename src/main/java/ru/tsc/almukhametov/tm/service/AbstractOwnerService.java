package ru.tsc.almukhametov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IOwnerRepository;
import ru.tsc.almukhametov.tm.api.service.IOwnerService;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public E add(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(userId, entity);
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(userId, entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Nullable
    @Override
    public Optional<E> findById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Nullable
    @Override
    public Optional<E> findByIndex(@NotNull final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public Optional<E> removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public Optional<E> removeByIndex(@NotNull final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.removeByIndex(userId, index);
    }


    @Override
    public boolean existById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existById(userId, id);
    }

    @Override
    public boolean existByIndex(@NotNull final String userId, final int index) {
        return repository.existByIndex(userId, index);
    }

}
