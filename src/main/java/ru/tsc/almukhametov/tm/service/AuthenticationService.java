package ru.tsc.almukhametov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IAuthenticationRepository;
import ru.tsc.almukhametov.tm.api.service.IAuthenticationService;
import ru.tsc.almukhametov.tm.api.service.IPropertyService;
import ru.tsc.almukhametov.tm.api.service.IUserService;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.EmptyLoginException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyPasswordException;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.Optional;

public final class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;
    @NotNull
    private final IAuthenticationRepository authenticationRepository;
    @NotNull
    private final IPropertyService propertyService;

    public AuthenticationService(@NotNull final IUserService userService,
                                 @NotNull final IAuthenticationRepository authenticationRepository,
                                 @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.authenticationRepository = authenticationRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public Optional<User> getCurrentUserId() {
        @Nullable final String userId = authenticationRepository.getCurrentUserId();
        if (userId == null) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void setCurrentUserId(@Nullable String userId) {
        authenticationRepository.setCurrentUserId(userId);
    }

    @Override
    public void checkRoles(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final Optional<User> user = getCurrentUserId();
        if (!user.isPresent()) throw new AccessDeniedException();
        @Nullable final Role role = user.get().getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuthentication() {
        @NotNull final String currentUserId = authenticationRepository.getCurrentUserId();
        return !(currentUserId == null || currentUserId.isEmpty());
    }

    @Override
    public void logout() {
        if (isAuthentication()) throw new AccessDeniedException();
        setCurrentUserId(null);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().isLocked()) throw new AccessDeniedException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @Nullable final String hash = HashUtil.salt(password, secret, iteration);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.get().getId());
    }

    @Override
    public void registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        userService.create(login, password, email);
    }

}
