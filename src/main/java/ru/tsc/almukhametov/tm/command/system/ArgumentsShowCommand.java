package ru.tsc.almukhametov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

import java.util.Collection;

public final class ArgumentsShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.ARGUMENTS;
    }

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARGUMENTS;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.ARGUMENTS;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand arg : arguments) {
            System.out.println(arg.name());
        }
    }

}
