package ru.tsc.almukhametov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;

public final class UserClearCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_CLEAR;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.USER_CLEAR;
    }

    @Override
    public void execute() {
        System.out.println("[ClEAR USERS]");
        serviceLocator.getUserService().clear();
        System.out.println("[SUCCESS CLEAR]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
