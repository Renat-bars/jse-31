package ru.tsc.almukhametov.tm.command.data.backup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.data.AbstractDataCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.dto.Domain;
import ru.tsc.almukhametov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadXmlCommand extends AbstractDataCommand {


    @NotNull
    @Override
    public String name() {
        return TerminalConst.BACKUP_LOAD_XML;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.BACKUP_LOAD_XML;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final File file = new File(FILE_BACKUP);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
