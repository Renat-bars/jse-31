package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Optional;
import java.util.function.Predicate;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    protected Predicate<Project> predicateByName(final String name) {
        return p -> name.equals(p.getName());
    }

    @NotNull
    @Override
    public Optional<Project> findByName(@NotNull final String userId, @Nullable final String name) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByName(name))
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<Project> removeByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Optional<Project> project = (findByName(userId, name));
        project.ifPresent(this::remove);
        return project;
    }

    @NotNull
    @Override
    public Project startById(@NotNull final String userId, @Nullable final String id) {
        @Nullable final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @NotNull
    @Override
    public Project startByIndex(@NotNull final String userId, final Integer index) {
        @Nullable final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @NotNull
    @Override
    public Project startByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.get();
    }

    @NotNull
    @Override
    public Project finishById(@NotNull final String userId, @Nullable String id) {
        @Nullable final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.get();
    }

    @NotNull
    @Override
    public Project finishByIndex(@NotNull final String userId, final Integer index) {
        @Nullable final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.get();
    }

    @NotNull
    @Override
    public Project finishByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.get();
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@NotNull final String userId, @Nullable String id, @NotNull Status status) {
        @Nullable final Optional<Project> project = findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(status));
        return project.get();
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(@NotNull final String userId, Integer index, @NotNull Status status) {
        @Nullable final Optional<Project> project = findByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(status));
        return project.get();
    }

    @NotNull
    @Override
    public Project changeProjectStatusByName(@NotNull final String userId, @Nullable String name, @NotNull Status status) {
        @Nullable final Optional<Project> project = findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(p -> p.setStatus(status));
        return project.get();
    }

}
