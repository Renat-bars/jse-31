package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UnknownStatusException extends AbstractException {

    public UnknownStatusException(@NotNull final String status) {
        super("Error! Sort ``" + status + "`` was not found");
    }

}
